import Vue, { PluginFunction, VueConstructor } from 'vue'

declare const QuestUi: PluginFunction<any>
export default QuestUi

export const QuestUiSample: VueConstructor<Vue>
